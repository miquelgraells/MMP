var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

// create directory to save pothos if it dosent exist
if (!fs.existsSync('../imagesRoutes/')){
    fs.mkdirSync('../imagesRoutes/');
}

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

//Set up MongoDB dependencies
// Database

var mongo = require('mongoskin');
var dbName = "mmp";
//var db = mongo.db("mongodb://"+process.env.OPENSHIFT_MONGODB_DB_HOST+":"+process.env.OPENSHIFT_MONGODB_DB_PORT+"/"+dbName, {native_parser:true});
var db = mongo.db("mongodb://"+process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT+"/"+dbName, {native_parser:true});

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

/*process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT*/

// session

app.use(session(
  {
    secret:'kdueo348cmsjfg037dk',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore (
      { 
            //url: 'mongodb://'+process.env.OPENSHIFT_MONGODB_DB_HOST+':'+process.env.OPENSHIFT_MONGODB_DB_PORT+'/' + dbName,
            url: 'mongodb://'+process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT+'/' + dbName,
            autoRemove: 'native'
        })
  })
);

console.log("===================MONGO INFO=========================")
console.log("dbmongo username ---"+process.env.OPENSHIFT_MONGODB_DB_USERNAME);
console.log("dbmongo password ---"+process.env.OPENSHIFT_MONGODB_DB_PASSWORD);
console.log("dbmongo host ---"+process.env.OPENSHIFT_MONGODB_DB_HOST);
console.log("dbmongo host ---"+process.env.OPENSHIFT_MONGODB_DB_PORT);
console.log("===================MONGO INFO=========================")

// Make db accessible to router
app.use(function(req,res,next){
    req.db = db;
    next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

