var refreshChartsInterval = 3000 // 3 sec

var temperature = 0;
var humidity = 0;
var CO = 0;

/* ****** replace this functions for a mqtt client ***** */
/* functions that generate random data just for test charts */
function randomTemp(){
    temperature = Math.round(Math.random() * (40 - -10) + -10);
}
function randomHumidity(){
    humidity = Math.round(Math.random() * (100 - 0) + -0)
}
function randomCO(){
    CO = Math.round(Math.random() * (300 - 0) + -0);
}
/* ****** replace this functions for a mqtt client ***** */


// ******* initizializing MQTT client ************
// example message to send : mosquitto_pub -h 'm12.cloudmqtt.com' -p 16844 -u 'holy' -P 'shit' -t '/nice' -m '{"temperature":35,"humidity":30,"CO":100}'
function initMqtt() {
    // Create a client instance
    client = new Paho.MQTT.Client("m12.cloudmqtt.com", 36844,"web_" + parseInt(Math.random() * 100, 10));   
    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
    var options = {
        useSSL: true,
        userName: "holy",
        password: "shit",
        onSuccess:onConnect,
        onFailure:doFail
    }
    // connect the client
    client.connect(options);
}

// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription.
    console.log("onConnect");
    client.subscribe("/nice");    
}

function doFail(e){
    console.log(e);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
    }
}
// ******* initizializing MQTT client ************

// called when a message arrives
function onMessageArrived(message) {
    var messageJSON =  JSON.parse(message.payloadString);
    // setting values when message arrives.
    temperature = messageJSON.temperature;
    humidity = messageJSON.humidity;
    CO = messageJSON.CO;
    console.log("onMessageArrived:"+message.payloadString);
}


$(document).ready(function () {
     
     //replace this functions for a mqtt client 
    setInterval(function () {        
        randomTemp();
        randomHumidity();
        randomCO(); 
    }, refreshChartsInterval);

    // initizialaizing MQTT client
    //initMqtt();

    // initizialaizing charts
    liveTempGraph ();
    liveHumidityGraph(); 
    liveCoGraph(); 

    liveTempGauge();
    liveHumidityGauge();
    liveCoGauge();
});

// shows the temp graph
function liveTempGraph (){
        Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    $('#temperatureGraph').highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            y = temperature; // this is the value updated from mqtt
                        series.addPoint([x, y], true, true);
                    }, refreshChartsInterval);
                }
            }
        },
        title: {
            text: 'Temperature live'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: '°C'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Temperature',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;
                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,         
                    });
                }
                return data;
            }())
        }]
    }); 
}

// shows humidity graph
function liveHumidityGraph() {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    $('#humidityGraph').highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            y = humidity; //// this is the value updated from mqtt
                        series.addPoint([x, y], true, true);
                    }, refreshChartsInterval);
                }
            }
        },
        title: {
            text: 'Humidity live'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: '%'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Random data',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;
                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,                     
                    });
                }
                return data;
            }())
        }]
    }); 
}

// shows the CO graph
function liveCoGraph() {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    $('#coGraph').highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            y = CO; // this is the value updated from mqtt
                        series.addPoint([x, y], true, true);
                    }, refreshChartsInterval);
                }
            }
        },
        title: {
            text: 'CO live'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'ppb'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Random data',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;
                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,                     
                    });
                }
                return data;
            }())
        }]
    }); 
}

// shows gauge temperature
function liveTempGauge() {
     $('#temperatureGauge').highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: ''
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: -10,
            max: 40,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: '°C'
            },
            plotBands: [{
                from: -10,
                to: 10,
                color: '#87CEFA' // blue
            }, {
                from: 10,
                to: 25,
                color: '#DDDF0D' // yellow
            }, {
                from: 25,
                to: 40,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: 'Temperature',
            data: [80],
            tooltip: {
                valueSuffix: ' °C'
            }
        }]

    },
    // Add some life
    function (chart) {
        if (!chart.renderer.forExport) {
            setInterval(function () {
                var point = chart.series[0].points[0],
                    newVal,                   

                newVal = temperature; // this is the value updated from mqtt
                if (newVal < -10 || newVal > 40) {
                    newVal = point.y - inc;
                }

                point.update(newVal);

            }, refreshChartsInterval);
        }
    });
}

// shows the humidity gauge
function liveHumidityGauge() {
         $('#humidityGauge').highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: ''
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 100,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: '%'
            },
            plotBands: [{
                from: 0,
                to: 30,
                color: '#87CEFA' // blue
            }, {
                from: 30,
                to: 75,
                color: '#DDDF0D' // yellow
            }, {
                from: 75,
                to: 100,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: 'Humidity',
            data: [80],
            tooltip: {
                valueSuffix: ' %'
            }
        }]

    },
    // Add some life
    function (chart) {
        if (!chart.renderer.forExport) {
            setInterval(function () {
                var point = chart.series[0].points[0],
                    newVal,
                    
                newVal = humidity; // this is the value updated from mqtt
                if (newVal < 0 || newVal > 100) {
                    newVal = point.y - inc;
                }

                point.update(newVal);

            }, refreshChartsInterval);
        }
    });
}

// shows the CO gauge
function liveCoGauge() {
    $('#coGauge').highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: ''
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 300,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: '%'
            },
            plotBands: [{
                from: 0,
                to: 100,
                color: '#87CEFA' // blue
            }, {
                from: 100,
                to: 200,
                color: '#DDDF0D' // yellow
            }, {
                from: 200,
                to: 300,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: 'CO',
            data: [80],
            tooltip: {
                valueSuffix: ' ppb'
            }
        }]

    },
    // Add some life
    function (chart) {
        if (!chart.renderer.forExport) {
            setInterval(function () {
                var point = chart.series[0].points[0],
                    newVal,

                newVal = CO; // this is the value updated from mqtt
                if (newVal < 0 || newVal > 300) {
                    newVal = point.y - inc;
                }

                point.update(newVal);

            }, refreshChartsInterval);
        }
    });
}
